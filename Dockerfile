FROM node
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN npm i
ENV PORT $PORT
CMD npm start